import numpy as np
import matplotlib.pyplot as plt

from scipy.signal import find_peaks
from multiprocessing import Pool
from cressman_rhs import solve_cressman_interval

from itertools import product


def cressman_wrapper(args):
    kbath, t0, t1 = args
    return solve_cressman_interval(t0=t0, t1=t1, kbath=kbath)


def kbath(T=100):
    """T is specified in seconds."""

    kbath_list = [2, 4, 6, 8, 9.5, 10]

    param_list = product(kbath_list, [0], [T*1e3])

    pool = Pool(processes=len(kbath_list))
    solutions = pool.map(cressman_wrapper, param_list)

    spikes = []

    for i, (k, sol) in enumerate(zip(kbath_list, solutions)):
        peaks = find_peaks(sol.y[0, :], height=0)[0]
        print(k, peaks.shape)
        spikes.append((k, peaks.shape))
        fig, ax = plt.subplots(1)
        ax.plot(sol.t, sol.y[0, :])
        ax.plot(sol.t[peaks], sol.y[0, peaks], "x")
        fig.savefig(f"tmp_figures/fig{i}.png")
        plt.close(fig)

    with open("num_spikes.txt", "a") as ofile:
        ofile.write("Kbath")
        ofile.write(f"T = {T} s\n")
        for k, s in spikes:
            ofile.write(f"k: {k}, \t\t s: {s}\n")
        ofile.write("\n")


def stim():
    sol = np.load("sol_stim.npy")
    time = np.load("time_stim.npy")

    peaks = find_peaks(sol[0, :], height=0)[0]

    fig, ax = plt.subplots(1)
    ax.plot(time, sol[0, :])

    ax.plot(time[peaks], sol[0, peaks], "x")
    # plt.show()
    print(peaks.shape)


def kbath10():
    time = np.load("time.npy")
    time_indices = time < 10000
    time = time[time_indices]

    sol = np.load("sol.npy")[0, time_indices]
    peaks = find_peaks(sol, height=0)[0]
    print(peaks.shape)

    fig, ax = plt.subplots(1)
    ax.plot(time, sol)
    ax.plot(time[peaks], sol[peaks], "x")



if __name__ == "__main__":
    kbath(10)
    # stim()
    kbath10()
