import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks


time = np.load("time.npy")
sol = np.load("sol.npy")

peaks = find_peaks(sol[0, :], height=0)[0]
print(peaks.shape)

fig, ax = plt.subplots(1)
ax.plot(time, sol[0, :])
ax.plot(time[peaks], sol[0, peaks], "x")

# plt.show()
