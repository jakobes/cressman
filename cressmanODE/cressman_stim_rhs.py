from scipy.integrate import solve_ivp

import numpy as np


CRESSMAN_DEFAULT_PARAMETERS = {
        "Cm": 1,
        "GNa": 100,
        "GK": 40,
        "GAHP": 0.01,
        "GKL": 0.05,
        "GNaL": 0.0175,
        "GClL": 0.05,
        "GCa": 0.1,
        "Gglia": 66,
        "Koinf": 4,     # Default = 4
        "gamma1": 0.0445,
        "tau": 1000,
        "control": 1,
        "period": 1000,
        "duration": 300,
        "amplitude": 3
}


CRESSMAN_DEFAULT_INITIAL_CONDITION = (-0, 0.0936, 0.96859, 0.08553, 0.0, 7.8, 15.5)


class RHS:
    def __init__(self, **kwargs):
        self.kwargs = dict(kwargs)
        if not callable(self.kwargs["Koinf"]):
            _tmp = self.kwargs["Koinf"]
            self.kwargs["Koinf"] = lambda x: _tmp

    def __call__(self, t, x, y):
        """Right hand side."""
        # Unpack varables
        kwargs = self.kwargs

        # Define some parameters
        rho = 1.25
        eps0 = 1.2
        beta0 = 7
        ECa = 120
        Cli = 6
        Clo = 130
        phi = 3

        # Algebraic expressions
        Ipump = rho*(1/(1 + np.exp((25 - y[6])/3)))*(1/(1 + np.exp(5.5 - y[5])))
        IGlia = kwargs["Gglia"]/(1 + np.exp((18 - y[5])/2.5))
        Idiff = eps0*(y[5] - kwargs["Koinf"](x))        # NB! Koinf varying in space
        Ki = 140 + (18 - y[6])
        Nao = 144 - beta0*(y[6] - 18)
        ENa = 26.64*np.log(Nao/y[6])
        EK = 26.64*np.log(kwargs["control"]*y[5]/Ki)
        ECl = 26.64*np.log(Cli/Clo)
        a_m = (3.0 + (0.1)*y[0])*(1 - np.exp(-3 - 1/10*y[0]))**(-1)
        b_m = 4*np.exp(-55/18 - 1/18*y[0])
        ah = (0.07)*np.exp(-11/5 - 1/20*y[0])
        bh = (1 + np.exp(-7/5 - 1/10*y[0]))**(-1)
        an = (0.34 + (0.01)*y[0])*(1 - np.exp(-17/5 - 1/10*y[0]))**(-1)
        bn = (0.125)*np.exp(-11/20 - 1/80*y[0])

        minf = a_m*(a_m + b_m)**(-1)
        taum = (a_m + b_m)**(-1)
        h_inf = (bh + ah)**(-1)*ah
        tauh = (bh + ah)**(-1)
        ninf = (an + bn)**(-1)*an
        taun = (an + bn)**(-1)

        INa = kwargs["GNa"]*y[1]**3*y[3]*(y[0] - ENa) + kwargs["GNaL"]*(y[0] - ENa)
        IK = (kwargs["GK"]*y[2]**4 + kwargs["GAHP"]*y[4]/(1 + y[4]) + kwargs["GKL"])*(y[0] - EK)
        ICl = kwargs["GClL"]*(y[0] - ECl)

        phi_ext = kwargs["duration"]*np.pi/kwargs["period"]
        I_ext = kwargs["amplitude"]*(1 + np.exp(10**2*((1 - y[7])*np.cos(phi_ext)
                      - y[8]*np.sin(phi_ext))))**(-1)
        omega = (2*np.pi)/kwargs["period"]

        dydt = (
            -(INa + IK + ICl - I_ext)/kwargs["Cm"],
            phi*(minf - y[1])*taum**(-1),
            phi*(ninf - y[2])/taun,
            phi*(h_inf - y[3])/tauh,
            -y[4]/80 - 0.002*kwargs["GCa"]*(y[0] - ECa)/(1 + np.exp(-(y[0] + 25)/2.5)),
            (kwargs["gamma1"]*beta0*IK - 2*beta0*Ipump - IGlia - Idiff)/kwargs["tau"],
            -(kwargs["gamma1"]*INa + 3*Ipump)/kwargs["tau"],
            y[7]*(1 - y[7]**2 - y[8]**2) - omega*y[8],
            y[8]*(1 - y[7]**2 - y[8]**2) + omega*y[7],
        )
        return np.asarray(dydt)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import time

    y0 = (-50, 0.0936, 0.96859, 0.08553, 0.0, 7.8, 15.5, 0, 1)
    # y0 = (-50, 0.0936, 0.96859, 0.08553, 0.0, 7.8, 15.5)
    kwargs = {
        "Cm": 1,
        "GNa": 100,
        "GK": 40,
        "GAHP": 0.01,
        "GKL": 0.05,
        "GNaL": 0.0175,
        "GClL": 0.05,
        "GCa": 0.1,
        "Gglia": 66,
        "Koinf": 4,     # Default = 4
        "gamma1": 0.0445,
        "tau": 1000,
        "control": 1,
        "period": 1000,
        "duration": 600,
        "amplitude": 3
    }
    interval = (0, 1e5)

    def rhs(t, y):
        _rhs = RHS(**kwargs)
        return _rhs(t, 0, y)

    tick = time.perf_counter()
    sol = solve_ivp(rhs, interval, y0, vectorized=True, method="BDF")
    tock = time.perf_counter()
    print(f"time: {tock - tick}")

    fig, ax = plt.subplots(1, constrained_layout=True)
    ax.plot(sol.t/1000, sol.y[0, :], label="V")
    ax.set_title("Periodic external stimulus", fontsize=16)
    ax.set_xlabel("time [s]", fontsize=16)
    ax.set_ylabel("mV", fontsize=16)

    # i = 0
    # for y in sol.y:
    #     ax.plot(sol.t, y, label=f"{i}")
    #     i += 1
    #     break
    ax.legend(fontsize=16, loc="upper right")

    np.save("time_stim", sol.t)
    np.save("sol_stim", sol.y)

    fig.savefig("plot_cressman.png")
