from scipy.integrate import solve_ivp

from welch import welch_psd

import numpy as np


CRESSMAN_DEFAULT_PARAMETERS = {
        "Cm": 1,
        "GNa": 100,
        "GK": 40,
        "GAHP": 0.01,
        "GKL": 0.05,
        "GNaL": 0.0175,
        "GClL": 0.05,
        "GCa": 0.1,
        "Gglia": 66,
        "Koinf": 4,     # Default = 4
        "gamma1": 0.0445,
        "tau": 1000,
        "control": 1,
}

CRESSMAN_DEFAULT_INITIAL_CONDITION = (-50, 0.0936, 0.96859, 0.08553, 0.0, 7.8, 15.5)


class RHS:
    def __init__(self, **kwargs):
        self.kwargs = dict(kwargs)
        if not callable(self.kwargs["Koinf"]):
            _tmp = self.kwargs["Koinf"]
            self.kwargs["Koinf"] = lambda x: _tmp

    def __call__(self, t, x, y):
        """Right hand side."""
        # Unpack varables
        kwargs = self.kwargs

        # Define some parameters
        rho = 1.25
        eps0 = 1.2
        beta0 = 7
        ECa = 120
        Cli = 6
        Clo = 130
        phi = 3

        # Algebraic expressions
        Ipump = rho*(1/(1 + np.exp((25 - y[6])/3)))*(1/(1 + np.exp(5.5 - y[5])))
        IGlia = kwargs["Gglia"]/(1 + np.exp((18 - y[5])/2.5))
        Idiff = eps0*(y[5] - kwargs["Koinf"](x))        # NB! Koinf varying in space

        Ki = 140 + (18 - y[6])
        Nao = 144 - beta0*(y[6] - 18)
        ENa = 26.64*np.log(Nao/y[6])
        EK = 26.64*np.log(kwargs["control"]*y[5]/Ki)
        ECl = 26.64*np.log(Cli/Clo)

        a_m = (0.1*y[0] + 3)/(1 - np.exp(- 0.1*y[0] - 3))
        b_m = 4*np.exp(-55/18 - 1/18*y[0])
        ah = (0.07)*np.exp(-11/5 - 1/20*y[0])
        bh = (1 + np.exp(-7/5 - 1/10*y[0]))**(-1)
        an = (0.34 + (0.01)*y[0])*(1 - np.exp(-17/5 - 1/10*y[0]))**(-1)
        bn = (0.125)*np.exp(-11/20 - 1/80*y[0])

        minf = a_m*(a_m + b_m)**(-1)
        taum = (a_m + b_m)**(-1)
        h_inf = (bh + ah)**(-1)*ah
        tauh = (bh + ah)**(-1)
        ninf = (an + bn)**(-1)*an
        taun = (an + bn)**(-1)

        INa = kwargs["GNa"]*y[1]**3*y[3]*(y[0] - ENa) + kwargs["GNaL"]*(y[0] - ENa)
        IK = (kwargs["GK"]*y[2]**4 + kwargs["GAHP"]*y[4]/(1 + y[4]) + kwargs["GKL"])*(y[0] - EK)
        ICl = kwargs["GClL"]*(y[0] - ECl)

        dydt = (
            -(INa + IK + ICl)/kwargs["Cm"],
            phi*(minf - y[1])*taum**(-1),
            phi*(ninf - y[2])/taun,
            phi*(h_inf - y[3])/tauh,
            -y[4]/80 - 0.002*kwargs["GCa"]*(y[0] - ECa)/(1 + np.exp(-(y[0] + 25)/2.5)),
            (kwargs["gamma1"]*beta0*IK - 2*beta0*Ipump - IGlia - Idiff)/kwargs["tau"],
            -(kwargs["gamma1"]*INa + 3*Ipump)/kwargs["tau"]
        )
        return np.asarray(dydt)


def solve_cressman_interval(t0, t1, kbath = 4):
    import time

    t = np.linspace(t0, t1, int((t1 - t0)*20))

    y0 = (-50, 0.0936, 0.96859, 0.08553, 0.0, 7.8, 15.5)
    kwargs = {
        "Cm": 1,
        "GNa": 100,
        "GK": 40,
        "GAHP": 0.01,
        "GKL": 0.05,
        "GNaL": 0.0175,
        "GClL": 0.05,
        "GCa": 0.1,
        "Gglia": 66,
        "Koinf": kbath,     # Default = 4
        "gamma1": 0.0445,
        "tau": 1000,
        "control": 1
    }
    interval = (t0, t1)

    rhs = RHS(**kwargs)
    foo = rhs(0.0, 0, y0)

    def rhs(t, y):
        _rhs = RHS(**kwargs)
        return _rhs(t, 0, y)

    tick = time.perf_counter()
    sol = solve_ivp(rhs, interval, y0, vectorized=True, atol=1e-10, rtol=1e-6, method="BDF", t_eval=t)
    tock = time.perf_counter()
    print(f"time: {tock - tick}")
    print(f"num steps: {sol.t.size}")
    print(f"last time: {sol.t[-1]}")
    return sol


def call(*, t1):
    y0 = (-50, 0.0936, 0.96859, 0.08553, 0.0, 7.8, 15.5)
    kwargs = {
        "Cm": 1,
        "GNa": 100,
        "GK": 40,
        "GAHP": 0.01,
        "GKL": 0.05,
        "GNaL": 0.0175,
        "GClL": 0.05,
        "GCa": 0.1,
        "Gglia": 66,
        "Koinf": 8,     # Default = 4
        "gamma1": 0.0445,
        "tau": 1000,
        "control": 1
    }
    rhs = RHS(**kwargs)
    # rhs(t1, x=0, y=CRESSMAN_DEFAULT_INITIAL_CONDITION)


if __name__ == "__main__":
    # call(t1=0.025)
    import matplotlib.pyplot as plt
    sol = solve_cressman_interval(0, 1e5)
    print(sol.y[:, -1])

    time = sol.t/1000      # Convert from ms to s
    dt = time[1] - time[0]

    frequencies, power_density = welch_psd(
        eeg_signal=sol.y[0, :],
        fs=1/dt,
        nperseg=500,
        log_scale=False
    )

    fig, (ax1, ax2) = plt.subplots(2, constrained_layout=True)
    ax1.plot(time, sol.y[0, :], linewidth=1)
    ax1.set_xlabel("time [s]")
    ax1.set_ylabel("mV")
    ax2.plot(frequencies, power_density)
    ax2.set_xscale("log")
    ax2.set_yscale("log")
    ax2.set_xlabel("Hz")
    ax2.set_ylabel("dB")


    # fig, ax = plt.subplots(1, constrained_layout=True)
    # ax.plot(sol.t/1000, sol.y[0, :], label="V")
    # ax.set_title("transmembrane potential on a stable trajectory", fontsize=16)
    # ax.set_xlabel("time [s]", fontsize=16)
    # ax.set_ylabel("mV", fontsize=16)




    # ax2 = ax.twinx()
    # ax2.plot(sol.t, sol.y[4, :], linewidth=1, linestyle="--", label="Ca")
    # ax2.plot(sol.t, sol.y[5, :], linewidth=1, linestyle="--", label="K")
    # ax2.plot(sol.t, sol.y[6, :], linewidth=1, linestyle="--", label="Na")

    # ax2.plot(sol.t, sol.y[1, :], linewidth=0.5, linestyle="--", label="m")
    # ax2.plot(sol.t, sol.y[2, :], linewidth=0.5, linestyle="--", label="n")
    # ax2.plot(sol.t, sol.y[3, :], linewidth=0.5, linestyle="--", label="h")


    # ca = sol.y[4, :]
    # k = sol.y[5, :]
    # na = sol.y[6, :]
    # print(ca.min(), ca.max())
    # print(k.min(), k.max())
    # print(na.min(), na.max())

    # i = 0
    # for i, y in enumerate(sol.y):
    #     ax.plot(sol.t, y, label=f"{i}")
    #     break
    # ax.legend(fontsize=16, loc="upper right")

    np.save("time", sol.t)
    np.save("sol", sol.y)

    fig.savefig("plot_cressman.png")
