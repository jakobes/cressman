
from setuptools import setup, find_packages


setup(
    name = "",
    author = "Jakob E. Schreiner",
    author_email = "jakob@xal.no",
    packages = ["cressmanODE"],
    package_dir = {"cressmanODE": "cressmanODE"},
    install_requires=[
        "numpy",
        "scipy",
        "matplotlib"
    ],
    dependency_links = [
        "gti+git://github.com/jaidevd/pyhht.git#egg=pyhht",
        "gti+git://github.com/laszukdawid/PyEMD.git#egg=EMD-signal",
    ]
)
