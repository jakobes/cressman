# cressman

@article{barreto2011ion,
  title={Ion concentration dynamics as a mechanism for neuronal bursting},
  author={Barreto, Ernest and Cressman, John R},
  journal={Journal of biological physics},
  volume={37},
  number={3},
  pages={361--373},
  year={2

# Solution with external stimulus

![Alt text](src/plot_cressman.png?raw=true)
